import java.io.IOException;
import java.io.Serializable;

public class Command implements Serializable {
    private String command;

    public Command(String command) {
        this.command = command;
    }

    public void execute() throws IOException {
        Runtime.getRuntime().exec(this.command);
    }
}
