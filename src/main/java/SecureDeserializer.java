import java.io.*;
import java.nio.charset.StandardCharsets;

public class SecureDeserializer {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        final String filename = "simple.obj";

        FileInputStream fis = new FileInputStream(filename);

        byte[] buffer = new byte[fis.available()];
        fis.read(buffer);
        String s = new String(buffer, StandardCharsets.UTF_8);
        if(!s.contains("SimpleObject")) {
            System.out.println("*** SECURITY WARNING ***");
            System.out.println("Aborting deserialization");
            System.exit(1);
        }
        fis.close();

        fis = new FileInputStream(filename);
        ObjectInputStream ois = new ObjectInputStream(fis);

        SimpleObject readObject = (SimpleObject) ois.readObject();
        System.out.println(readObject.getName());
    }
}
