import org.apache.commons.collections4.functors.InvokerTransformer;

import java.io.*;

public class App {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        SimpleObject simpleObject = new SimpleObject("prova");
        System.out.println(simpleObject.getName());

        FileOutputStream fos = new FileOutputStream("simple.obj");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(simpleObject);
        oos.close();

        FileInputStream fis = new FileInputStream("simple.obj");
        ObjectInputStream ois = new ObjectInputStream(fis);

        SimpleObject readObject = (SimpleObject) ois.readObject();
        System.out.println(readObject.getName());
    }

}
