import java.io.Serializable;

public class SimpleObject implements Serializable {

    private String name;

    public SimpleObject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
